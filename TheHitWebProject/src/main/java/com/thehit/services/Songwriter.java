package com.thehit.services;

import com.thehit.domain.Song;

public interface Songwriter {
	void compose(Song song);
}
