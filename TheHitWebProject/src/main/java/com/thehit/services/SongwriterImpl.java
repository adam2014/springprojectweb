package com.thehit.services;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.thehit.domain.Person;
import com.thehit.domain.Song;

@Component("beanCustomName")
public class SongwriterImpl extends Person implements Songwriter {

	@Autowired
	private Song song;

	private int id;

	// I moved all the validation messages to the properties file 
	// so that the messages can be localized in future
	@NotNull
	private boolean newsletter = false;

	@NotNull
	private List<String> interests;

	@NotNull
	private String favouriteWord;

	public SongwriterImpl() {
		super();
	}

	public SongwriterImpl(String firstname, String lastname, int age, Song song) {
		super(firstname, lastname, age);

		this.song = song;
		System.out.println("fistname: " + firstname + " lastname: " + lastname
				+ " age: " + age + " song name: " + song.getName()
				+ " song lyrics: " + song.getLyrics());

	}

	public SongwriterImpl(@Value("${firstname}") String firstname,
			@Value("${lastname}") String lastname, @Value("${age}") int age) {
		super(firstname, lastname, age);

	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

	@Override
	public void compose(Song song) {
		System.out.println("Composer " + song.getName()
				+ " composed a song called " + song.getName()
				+ ". This song has the following lyrics " + song.getLyrics());

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}

	public List<String> getInterests() {
		return interests;
	}

	public void setInterests(List<String> interests) {
		this.interests = interests;
	}

	public String getFavouriteWord() {
		return favouriteWord;
	}

	public void setFavouriteWord(String favouriteWord) {
		this.favouriteWord = favouriteWord;
	}

	@Override
	public String toString() {
		return "SongwriterImpl [song=" + song + ", id=" + id + ", newsletter="
				+ newsletter + ", interests=" + interests + ", favouriteWord="
				+ favouriteWord + "]";
	}
	
}
