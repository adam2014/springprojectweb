package com.thehit.services;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thehit.domain.Song;

@Service
public interface SongDAO {
	
	public void setDataSource(DataSource ds);

	public KeyHolder createSong(String name, String lyrics);

	public int deleteSong(Integer id);

	public int updateSong(Integer id, String lyrics);

	public Song getSong(Integer id);
	
	public Song getSong(String name);

	public List<Song> listSongs();

	public int countRows();
	
	public List<Song> listSongs(String writerFirstname, String writerLastname); 
	
	public List<Song> listSongsOfSongWriterWithAgeGreaterThan(int age);
	   
}
