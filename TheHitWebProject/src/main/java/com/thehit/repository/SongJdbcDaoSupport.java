package com.thehit.repository;

import java.util.List;

import javax.sql.DataSource;

import org.hsqldb.types.Types;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.thehit.domain.Song;
import com.thehit.domain.SongMapper;
import com.thehit.services.SongDAO;

/*
 * 
 * @Author adan abdulrehman R00118124
 * 
 */

// add a layer which also creates a Bean called songJdbcDaoSupport
// the annotation Repository is a subclass of the Component annotation
// so we inherit its functionality.
// The Repository annotation also dictates that this is functionality
// based on Databases
@Repository
public class SongJdbcDaoSupport extends JdbcDaoSupport implements SongDAO {

	@Autowired
	SongJdbcDaoSupport(DataSource dataSource) {
		setDataSource(dataSource);
	}

	public KeyHolder createSong(String name, String lyrics) {
		String SQL = "insert into Song (songname, lyrics) values (?, ?)";

		Object[] params = new Object[] { name, lyrics };
		PreparedStatementCreatorFactory psc = new PreparedStatementCreatorFactory(
				SQL);
		psc.addParameter(new SqlParameter("name", Types.VARCHAR));
		psc.addParameter(new SqlParameter("lyrics", Types.VARCHAR));

		KeyHolder holder = new GeneratedKeyHolder();
		this.getJdbcTemplate().update(psc.newPreparedStatementCreator(params),
				holder);
		return holder;
	}

	public int deleteSong(Integer id) {
		String SQL = "delete from Song where idSong = ?";
		
		return getJdbcTemplate().update(SQL, new Object[] { id });
	}

	public int updateSong(Integer id, String lyrics) {
		String SQL = "UPDATE Song SET lyrics = ? WHERE idSong = ?";
		return getJdbcTemplate().update(SQL, new Object[] { lyrics, id });
	}

	public Song getSong(Integer id) {
		String SQL = "SELECT * FROM song WHERE idSong = ?";
		Song song = (Song) getJdbcTemplate()
				.queryForObject(SQL, new Object[] { id },
						new SongMapper());
		return song;
	}

	public List<Song> listSongs() {
		String SQL = "SELECT * FROM song";
		List<Song> songList = getJdbcTemplate().query(SQL, new SongMapper());
		return songList;
	}

	public int countRows() {
		String SQL = "select count(*) from Song";
		int rows=getJdbcTemplate().queryForObject(SQL, Integer.class);
		return rows;
	}

	public List<Song> listSongs(String songwriterFirstname, String songwriterLastname) {
		List<Song> songs;
		String SQL = "SELECT * from Song"
				+ " JOIN songwriter on songwriter.idsong=song.idsong "
				+ " AND songwriter.firstname= ?" + " AND songwriter.lastname=?";
		songs = getJdbcTemplate().query(SQL,
				new Object[] { songwriterFirstname, songwriterLastname },
				new SongMapper());

		return songs;

	}
	
	public List<Song> listSongsOfSongWriterWithAgeGreaterThan(int age) {
		List<Song> songs;
		String SQL="SELECT * from song"
				+ " JOIN songwriter on songwriter.idsong=song.idsong "
				+ " AND songwriter.age > ?";
		songs = getJdbcTemplate().query(SQL, new Object[] {age}, new SongMapper());
		
		return songs;  
	}

	@Override
	public Song getSong(String name) {
		String SQL = "SELECT * FROM song WHERE songname = ?";
		Song song = (Song) getJdbcTemplate()
				.queryForObject(SQL, new Object[] { name },
						new SongMapper());
		return song;
	}
}
