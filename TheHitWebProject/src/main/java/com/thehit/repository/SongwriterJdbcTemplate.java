
package com.thehit.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.thehit.domain.SongwriterMapper;
import com.thehit.services.SongwriterDAO;
import com.thehit.services.SongwriterImpl;

/*
 * 
 * @Author adan abdulrehman R00118124
 * 
 */
@Repository
public class SongwriterJdbcTemplate implements SongwriterDAO {
	@Autowired
	private DataSource dataSource;	
	private JdbcTemplate jdbcTemplateObject;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void createSongwriter(final String firstname, final String lastname, final Integer age) {
	String SQL = "insert into Songwriter (firstname, lastname, age) values (?, ?, ?)";
		
		Object[] params=new Object[]{ firstname, lastname, age};
		
		PreparedStatementCreatorFactory psc=new PreparedStatementCreatorFactory(SQL);
		psc.addParameter(new SqlParameter("firstname", Types.VARCHAR));
		psc.addParameter(new SqlParameter("lastname", Types.VARCHAR));
		psc.addParameter(new SqlParameter("age", Types.INTEGER));
		
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplateObject.update(psc.newPreparedStatementCreator(params), holder);
		
		return;	
	}

	@Override
	public void deleteSongwriter(Integer id) {
		String SQL = "delete from Songwriter where id = ?";
		jdbcTemplateObject.update(SQL, new Object[] {id});
		System.out.println("Deleted Record with ID = " + id );
		return;	
	}

	@Override
	public void updateSongwriter(Integer id, Integer age) {
		String SQL = "update Songwriter set age = ? where id = ?";
		jdbcTemplateObject.update(SQL,  new Object[] {age, id});
		System.out.println("Updated Record with ID = " + id );
		return;		
	}

	@Override
	public void deleteSongwriter(String firstname, String lastname) {
		String SQL = "delete from Songwriter where firstname = ? and lastname = ?";
		jdbcTemplateObject.update(SQL, new Object[] {firstname, lastname});
		System.out.println("Deleted Record with ID = " + firstname );
		return;
		
	}

	@Override
	public void updateSongwriter(String firstname, String lastname, Integer age) {
		String SQL = "update Songwriter set age = ? where firstname = ? and lastname = ?";
		jdbcTemplateObject.update(SQL,  new Object[] {age, firstname, lastname});
		System.out.println("Updated Record with ID = " + firstname );
		return;			
	}

	@Override
	public SongwriterImpl getSongwriter(Integer id) {
		String SQL = "select * from songwriter where idSongwriter = ?";
		SongwriterImpl songwriter = (SongwriterImpl) jdbcTemplateObject.queryForObject(SQL, 
						new Object[]{id}, new SongwriterMapper());
		return songwriter;
	}

	@Override
	public List<SongwriterImpl> listSongwriters() {
		String SQL = "select * from songwriter";
		List<SongwriterImpl> songwriterList = jdbcTemplateObject.query(SQL, 
						new SongwriterMapper());
		return songwriterList;
	}
	
	@Override
	public List<SongwriterImpl> listSongwriters(String firstname, String lastname) {
		String SQL = "select * from songwriter where firstname=? and lastname=?";
		List<SongwriterImpl> songwriterList = jdbcTemplateObject.query(SQL, new Object[]{firstname, lastname},
						new SongwriterMapper());
		return songwriterList;
	}

	@Override
	public void batchUpdate(final List<SongwriterImpl> songwriters) {
		String SQL = "insert into songwriter (firstname, lastname, age) values (?, ?, ?)";
		jdbcTemplateObject.batchUpdate(SQL, new BatchPreparedStatementSetter() {

			public int getBatchSize() {
				return songwriters.size();
			}

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SongwriterImpl songwriter = songwriters.get(i);
				ps.setString(1, songwriter.getFirstname());
				ps.setString(2, songwriter.getLastname());
				ps.setInt(3, songwriter.getAge() );
			}			
		});
		
	}

	@Override
	public int countRows() {
		String SQL = "select count(*) from songwriter";
		int rows=jdbcTemplateObject.queryForObject(SQL, Integer.class);
		return rows;  
	}

	@Override
	public List<SongwriterImpl> listSongWriters(String songName) {
		List<SongwriterImpl> songwriters;
		String SQL="SELECT * from Songwriter"
				+ " JOIN song on song.idsong=songwriter.idsong "
				+ " AND song.songname= ?";
		songwriters = jdbcTemplateObject.query(SQL, new Object[] {songName}, new SongwriterMapper());
		
		return songwriters;   
	}

	
}

	