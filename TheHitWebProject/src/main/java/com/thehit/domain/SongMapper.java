package com.thehit.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.thehit.domain.Song;

/*
 * This is the SongMapper class to provide for mapping
 * Database results to the Song object (ORM type of thing)
 * 
 * 
 * @Author adan abdulrehman R00118124
 * 
 */
@Component
public class SongMapper implements RowMapper<Song> {
	public Song mapRow(ResultSet rs, int rowNum) throws SQLException {
		Song song = new Song(); 
		
		song.setId(rs.getInt("idSong"));
		song.setName(rs.getString("songname"));
		song.setLyrics(rs.getString("lyrics"));
		
		return song;
	}
}