package com.thehit.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Person {

	// I moved all the validation messages to the properties file 
	// so that the messages can be localized in future
	
	@Size(min=2, max=30) 
	private String firstname;
	
	@Size(min=2, max=30) 
	private String lastname;
	
	@NotNull
	@Min(value=13) 
	@Max(value=110)
	private int age;
	
	public Person(){}
	
	public Person(String firstname, String lastname, int age){
		this.firstname=firstname;
		this.lastname=lastname;
		this.age=age;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
}
