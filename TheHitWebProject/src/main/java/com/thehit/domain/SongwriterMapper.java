package com.thehit.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.thehit.services.SongwriterImpl;

/*
 * This is the Songwriter Mapper class to provide for mapping
 * Database results to the Songwriter object.
 * 
 * 
 * @Author adan abdulrehman R00118124
 * 
 */
@Component 
public class SongwriterMapper implements RowMapper<SongwriterImpl>{

	 public SongwriterImpl mapRow(ResultSet rs, int rowNum) throws SQLException {
		 SongwriterImpl songwriter = new SongwriterImpl();
	     songwriter.setId(rs.getInt("idSongwriter"));
	     songwriter.setFirstname(rs.getString("firstname"));
	     songwriter.setLastname(rs.getString("lastname"));
	     songwriter.setAge(rs.getInt("age"));
	     return songwriter;
	 }

}       