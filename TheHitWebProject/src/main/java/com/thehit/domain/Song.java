package com.thehit.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
public class Song {

	// I moved all the validation messages to the properties file 
	// so that the messages can be localized in future
	
	@Size(min=2, max=44) 
	private String name;
	
	@Size(min=2, max=44) 
	private String lyrics;
	
	// we could check many many things such as a number is entered,
	// if it is in the valid format but for demo purposes here we
	// are going to just check not null and that some digits are entered
	// please just check say -1 or 0 value for example to see validation
	@NotNull
	@Min(1)
	private int id;
	
	public Song(String name,String lyrics){
		this.name=name;
		this.lyrics=lyrics;
	}
	
	public Song(){
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLyrics() {
		return lyrics;
	}
	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Song id is " + id + " - Song name is " + name + ", lyrics are - " + lyrics;
	}

}
