package com.thehit.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.thehit.services.SongwriterDAO;
import com.thehit.services.SongwriterImpl;

/*
 * This is the Songwriter Controller class following
 * the lecturers notes
 * 
 * 
 * @Author adan abdulrehman R00118124
 * 
 */

@Controller
@RequestMapping("/songwriter")
public class SongwriterController {
	@Autowired
	SongwriterDAO songwriterDAO;

	List<String> interestsList = new ArrayList<String>();

	public SongwriterController() {
		interestsList.add("interest1");
		interestsList.add("interest2");
		interestsList.add("interests3");
	}

	// This method lists all songwriters
	@RequestMapping(value = "/listallSongWriters", method = RequestMethod.GET)
	public String listAll(ModelMap model) {

		List<SongwriterImpl> listSongwriters = songwriterDAO.listSongwriters();
		model.addAttribute("songwriters", listSongwriters);

		String now = (new java.util.Date()).toString();
		model.addAttribute("now", now);

		return "displaySongwriters";
	}

	// This method lists all songwriters with the given first and last name
	@RequestMapping(value = "/list/{firstname}/{lastname}", method = RequestMethod.GET)
	public String findSinger(@PathVariable String firstname,
			@PathVariable String lastname, ModelMap model) {
		List<SongwriterImpl> listSongwriters = songwriterDAO.listSongwriters(
				firstname, lastname);

		for (SongwriterImpl songwriter : listSongwriters) {
			model.addAttribute("firstname", songwriter.getFirstname());
			model.addAttribute("lastname", songwriter.getLastname());
			model.addAttribute("age", songwriter.getAge());
		}
		return "displaySinger";
	}

	// This method shows the form to add a new songwriter
	@RequestMapping(value = "/addNew", method = RequestMethod.GET)
	public ModelAndView addNewSongwriter() {

		ModelMap map = new ModelMap();
		map.addAttribute("songwriter", new SongwriterImpl());
		map.addAttribute("interests", interestsList);
		return new ModelAndView("newSongwriter", map);
	}
	
	// This method shows the form used for listing all the songs for a 
	// songwriter with the given first and last name
	@RequestMapping(value = "/listSongsBySongwriter", method = RequestMethod.GET)
	public ModelAndView listSongsOfSongwriter() {

		ModelMap map = new ModelMap();
		map.addAttribute("firstname", new String());
		map.addAttribute("lastname", new String());
		return new ModelAndView("listSongForm", map);
	}

	// This method lists all songwriters
	@RequestMapping(value = "/listSongsBySongwriter", method = RequestMethod.POST)
	public String listSongsOfSongwriter(@ModelAttribute String firstname,
			@ModelAttribute String lastname, ModelMap model) {
		List<SongwriterImpl> listSongwriters = songwriterDAO.listSongwriters(
				firstname, lastname);

		for (SongwriterImpl songwriter : listSongwriters) {
			model.addAttribute("firstname", songwriter.getFirstname());
			model.addAttribute("lastname", songwriter.getLastname());
			model.addAttribute("age", songwriter.getAge());
		}
		return "displaySongwriters";
	}

	// This method cretaes a new songwriter
	@RequestMapping(value = "/addNew", method = RequestMethod.POST)
	public String displaySongwriter(
			@ModelAttribute("songwriter") @Valid SongwriterImpl songwriter,
			BindingResult result, ModelMap model) {

		if (result.hasErrors())
			return "newSongwriter";

		model.addAttribute("firstname", songwriter.getFirstname());
		model.addAttribute("lastname", songwriter.getLastname());
		model.addAttribute("age", songwriter.getAge());
		model.addAttribute("interests", songwriter.getInterests());
		model.addAttribute("favouriteWord", songwriter.getFavouriteWord());
		model.addAttribute("newsletter", songwriter.isNewsletter());

		try {
			songwriterDAO.createSongwriter(songwriter.getFirstname(),
					songwriter.getLastname(), songwriter.getAge());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "displaySongwriter";
	}

	/*
	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {

		List<SongwriterImpl> listSongwriters = songwriterDAO.listSongwriters();

		if (listSongwriters != null && listSongwriters.size() > 0)
			for (SongwriterImpl songwriter : listSongwriters)
				System.out.println(songwriter.getFirstname() + " "
						+ songwriter.getLastname());

		model.addAttribute("message",
				"Spring 3 MVC Hello World. This is HelloControllerClass.");
		return "hello";
	}
    */
}
