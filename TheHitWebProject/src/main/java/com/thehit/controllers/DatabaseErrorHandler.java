package com.thehit.controllers;

import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/*
 * Global Handler for ALL controllers to deal with database exception
 * the view that will be displayed is error.jsp
 * Other exceptions can be handled here to by including other methods
 * that handle specific exceptions and even have different views
 * the Advice part in the annotation I think has something to do with
 * Aspect oriented programming which I did not have time to explore
 * 
 * @author Adan Abdulrehman R00118124
 */

@ControllerAdvice
public class DatabaseErrorHandler {

	@ExceptionHandler(DataAccessException.class)
	public String handleDatabaseException(DataAccessException exception) {
		return "error";
	}

}
