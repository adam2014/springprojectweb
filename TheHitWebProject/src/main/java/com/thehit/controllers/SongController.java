package com.thehit.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.thehit.domain.Song;
import com.thehit.services.SongDAO;

/*
 * This is the Song Controller class
 * 
 * Note: there is no need to catch the exceptions caused by 
 * the database as a custom Exception handler has been included
 * 
 * Methods include those that:
 * List a song based on its ID.
 * List a song based on its name.
 * Delete a song based on its ID.
 * Update a song based on its ID with new lyrics.
 * List all songs.
 * 
 * @Author adan abdulrehman R00118124
 * 
 */
@Controller("songController")
@RequestMapping("/song")
public class SongController {
	@Autowired
	SongDAO songDAO;

	// This method Lists all songs.
	@RequestMapping(value = "/listSongs", method = RequestMethod.GET)
	public String listAllSongs(ModelMap model) {

		List<Song> songs = songDAO.listSongs();

		model.addAttribute("songs", songs);

		return "songDisplaySongs";
	}

	// This method shows the form for updating a song based on its ID
	// with new lyrics.
	@RequestMapping(value = "/updateSong", method = RequestMethod.GET)
	public ModelAndView getSongNameAndLyrics() {
		return new ModelAndView("songGetNameAndLyrics", "song", new Song());
	}

	// This method Updates a song based on its ID with new lyrics.
	@RequestMapping(value = "/updateSong", method = RequestMethod.POST)
	public String updateSongResults(@ModelAttribute("song") @Valid Song song,
			BindingResult result, ModelMap model) {

		if (result.hasErrors())
			return "songGetNameAndLyrics";

		int rowsAffected = songDAO.updateSong(song.getId(), song.getLyrics());

		if (rowsAffected > 0) {
			model.addAttribute("message", "Song Updated");
		} else {
			model.addAttribute("message",
					"Song was not updated. There was no song as such or Db went mental");
		}
		return "songMessage";
	}

	// This method shows the form for getting the song id to list it
	@RequestMapping(value = "/listSongById", method = RequestMethod.GET)
	public ModelAndView getSongId() {
		return new ModelAndView("songGetId", "song", new Song());
	}

	// This method lists a song based on its ID.
	@RequestMapping(value = "/listSongById", method = RequestMethod.POST)
	public String listSongByIdResults(@ModelAttribute("song") @Valid Song song,
			BindingResult result, ModelMap model) {

		if (result.hasErrors())
			return "songGetId";

		song = songDAO.getSong(song.getId());

		if (song != null) {
			model.addAttribute("song", song);
			model.addAttribute("message", "Song found");
		} else {
			model.addAttribute("message", "Song was not found");
		}
		return "songShowSong";
	}

	// This method gives the total number of songs
	@RequestMapping(value = "/songCount", method = RequestMethod.GET)
	public String songCount(ModelMap model) {

		int numberOfSongs = songDAO.countRows();
		model.addAttribute("numberOfSongs", numberOfSongs);
		model.addAttribute("message", "Songs found.");

		return "songCount";
	}

	// This method shows the form for deleting a song based on its ID
	@RequestMapping(value = "/deleteSongById", method = RequestMethod.GET)
	public ModelAndView getSongIdToDelete() {
		return new ModelAndView("songGetIdToDelete", "song", new Song());
	}

	// This method Updates a song based on its ID with new lyrics.
	@RequestMapping(value = "/deleteSongById", method = RequestMethod.POST)
	public String deleteSongResults(@ModelAttribute("song") @Valid Song song,
			BindingResult result, ModelMap model) {

		if (result.hasErrors())
			return "songGetIdToDelete";

		int rowsAffected = songDAO.deleteSong(song.getId());

		if (rowsAffected > 0) {
			model.addAttribute("message", "Song deleted");
		} else {
			model.addAttribute("message",
					"Song was not delete. There was no song as such or Db went mental");
		}
		return "songMessage";
	}

	// This method shows the form for getting the song name to list it
	@RequestMapping(value = "/listSongByName", method = RequestMethod.GET)
	public ModelAndView getSongName() {
		return new ModelAndView("songGetName", "song", new Song());
	}

	// This method lists a song based on its ID.
	@RequestMapping(value = "/listSongByName", method = RequestMethod.POST)
	public String listSongByNameResults(@ModelAttribute("song") @Valid Song song,
			BindingResult result, ModelMap model) {

		if (result.hasErrors())
			return "songGetName";

		song = songDAO.getSong(song.getName());

		if (song != null) {
			model.addAttribute("song", song);
			model.addAttribute("message", "Song found");
		} else {
			model.addAttribute("message", "Song was not found");
		}
		return "songShowSong";
	}

}
