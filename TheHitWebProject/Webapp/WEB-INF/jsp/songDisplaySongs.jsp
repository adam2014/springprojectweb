<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>    
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
    <body>
        <h1 id="banner">SONGS</h1> 
		<h1><fmt:message key="heading"/></h1>
		<h3><fmt:message key="greeting"/>${now}</h3>
		<h3><fmt:message key="title"/></h3>
		<h3>${message}</h3>
		
		<c:if test="${not empty songs}"> 
			<table>
				<c:forEach var="song" items="${songs}">		
					<tr>
	           			<td>ID: ${song.id}</td>
	            		<td>NAME: ${song.name}</td>
	            		<td>LYRICS: ${song.lyrics}</td>
	        		</tr>
				</c:forEach> 
			</table>
		</c:if>
			
    </body>
</html>