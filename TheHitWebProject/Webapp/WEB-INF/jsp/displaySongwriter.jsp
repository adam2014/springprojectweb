<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Song writer</title>
</head>
<body>
    <a href="<c:url value="/j_spring_security_logout" />" >Logout</a> 
	<h2>Songwriter Details</h2>
	<table>
		<tr>
			<td>Firstname</td>
			<td>${firstname}</td>
		</tr>

		<tr>
			<td>Lastname</td>
			<td>${lastname}</td>
		</tr>

		<tr>
			<td>Age</td>
			<td>${age}</td>
		</tr>
		<tr>
			<td>Favourite Word</td>
			<td>${favouriteWord}</td>
		</tr>

		<tr>
			<td>Newsletter</td>
			<td>${newsletter}</td>
		</tr>

		<tr>
			<c:forEach var="interest" items="${interests}">
				<td>Interest</td>
				<td>${interest}</td>
			</c:forEach>
		</tr>
	</table>
</body>
</html>