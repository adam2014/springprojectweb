<%@ include file="/WEB-INF/jsp/include.jsp"%>

<html>
<head>
<title>Add New Songwriter</title>
</head>
<body>
    <a href="<c:url value="/j_spring_security_logout" />" >Logout</a> 
	<form:form method="POST" action="addNew" modelAttribute="songwriter">
		<table>
			<tr>
				<td colspan="2">Add a new Songwriter.</td>
			</tr>
			<tr>
				<td><form:label path="firstname">Firstname</form:label></td>
				<td><form:input path="firstname" /> 
				<form:errors path="firstname" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="lastname">Lastname</form:label></td>
				<td><form:input path="lastname" /> 
				<form:errors path="lastname" cssclass="error"></form:errors></td>
			</tr>
			<tr>
				<td><form:label path="age">Age</form:label></td>
				<td><form:input path="age" /> 
				<form:errors path="age" cssclass="error"></form:errors></td>
			</tr>

			<tr>
				<td>Subscribe to newsletter?:</td>
				<%-- Approach 1: Property is of type java.lang.Boolean --%>
				<td><form:checkbox path="newsletter" /> 
				<form:errors path="newsletter" cssclass="error"></form:errors></td>
			</tr>

			<tr>
				<td>Interests:</td>
				<td>
					<%-- Approach 2: Property is of an array or of type java.util.Collection --%>
					Quidditch: <form:checkbox path="interests" value="Quidditch" />
					Herbology: <form:checkbox path="interests" value="Herbology" />
					Defence Against the Dark Arts: <form:checkbox path="interests" value="Defence Against the Dark Arts" /> 
						<form:errors path="interests" cssclass="error"></form:errors>
				</td>
			</tr>
			<tr>
				<td>Favourite Word:</td>
				<td>
					<%-- Approach 3: Property is of type java.lang.Object --%> Magic: <form:checkbox
						path="favouriteWord" value="Magic" /> 
						<form:errors path="favouriteWord" cssclass="error"></form:errors>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Add Songwriter" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
