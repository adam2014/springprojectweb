<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>    
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
    <body>
        <h1 id="banner">SONGS</h1> 
		<h3><fmt:message key="heading"/></h3>
		<h3><fmt:message key="title"/></h3>
		<h4>${message}</h4>
		
		<h2>ID : ${song.id}</h2>
		<h2>Name : ${song.name}</h2>
		<h2>Lyrics : ${song.lyrics}</h2>

    </body>
</html>