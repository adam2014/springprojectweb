<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Song writer Display</title>
</head>
<body>
<h1>Spring 3 MVC REST web service</h1>
    <a href="<c:url value="/j_spring_security_logout" />" >Logout</a> 
	<h2>Songwriter : ${firstname}</h2>
	<h2>Songwriter lastname : ${lastname}</h2>
	<h2>Songwriter age : ${age}</h2>
</body>
</html>