<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <body>    
        <h1 id="banner">Unauthorized Access !!</h1>     
        <hr/>   	
     	
        <c:if test="${not empty error}">
            <div style="color:red">
                Your fake login attempt was busted, dare again !!<br />
                Caused : Invalid username or password.
            </div>
        </c:if>
     
        <p class="message">Access denied!</p>
        <a href="/TheHitWebProject/login">Go back to login page</a>
    </body>
</html>                              