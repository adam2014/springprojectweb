<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>List singers example</title>
</head>
<body>
	<h3><fmt:message key="heading"/></h3>
	<h4><fmt:message key="title"/></h4>

	<c:if test="${not empty songwriters}"> 
		<table>
			<c:forEach var="songwriter" items="${songwriters}">
				<tr>
           			<td>${songwriter.firstname}</td>
            		<td>${songwriter.lastname}</td>
            		<td> Age - ${songwriter.age}</td>
        	</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>