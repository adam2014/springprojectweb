<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>    
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
    <body>
        <h2 id="banner">SONGS - SEARCH</h2> 
		<h3><fmt:message key="heading"/></h3>
		<h3><fmt:message key="title"/></h3>
		<h3>${message}</h3>
		
		<form:form method="POST" action="listSongByName" modelAttribute="song">
			<table> 
				<tr>
					<td colspan="2">Find a song.</td>
				</tr>
				<tr>
					<td><form:label path="name">Song name</form:label></td>
					<td><form:input path="name" /> 
					<form:errors path="name" cssclass="error"></form:errors></td> 
				</tr>
	
				<tr>
					<td colspan="2"><input type="submit" value="Find song" /></td>
				</tr>
			</table>
		</form:form>
    </body>
</html>
