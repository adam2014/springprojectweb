<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Menu</h2>
	<br>
	<a href="/TheHitWebProject/songwriter/listallSongWriters">List Singers</a>

	<br>
	<a href="/TheHitWebProject/songwriter/addNew">Add Singer</a>

	<br>
	<a href="/TheHitWebProject/song/updateSong">Update a song</a>
	
	<br>
	<a href="/TheHitWebProject/song/listSongs">List all songs</a>
	
	<br>
	<a href="/TheHitWebProject/song/listSongById">List song by Id</a>
	
	<br>
	<a href="/TheHitWebProject/song/listSongByName">List song by name</a>
	
	<br>
	<a href="/TheHitWebProject/song/deleteSongById">Delete song by Id</a>
	
	<br>
	<a href="/TheHitWebProject/song/songCount">Total number of songs</a>
	
	<br>
	<a href="/TheHitWebProject/j_spring_security_logout" >Logout</a> 

</body>
</html>