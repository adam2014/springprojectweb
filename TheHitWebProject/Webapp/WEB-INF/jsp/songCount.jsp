<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>    
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
    <body>
        <h1 id="banner">SONGS - TOTAL SONG COUNT</h1> 
		<a href="<c:url value="/j_spring_security_logout" />" >Logout</a> 
		<h1><fmt:message key="heading"/></h1>
		<h3><fmt:message key="greeting"/>${now}</h3>
		<h3><fmt:message key="title"/></h3>
		<h3>${message}</h3>
		
		<h2>Total number of songs is: ${numberOfSongs}</h2>

    </body>
</html>
